#ifndef __PROGTEST__
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>
using namespace std;

class CTimeStamp
{
public:
    CTimeStamp                              ( int               year,
                                              int               month,
                                              int               day,
                                              int               hour,
                                              int               minute,
                                              double            sec );
    int            Compare                                 ( const CTimeStamp & x ) const;
    friend ostream & operator <<                           ( ostream          & os,
                                                             const CTimeStamp & x );
private:
    int m_year;
    int m_month;
    int m_day;
    int m_hour;
    int m_minute;
    double m_sec;
};

CTimeStamp::CTimeStamp(int year, int month, int day, int hour, int minute, double sec):
m_year(year), m_month(month), m_day(day), m_hour(hour), m_minute(minute), m_sec(sec)
{

}

int CTimeStamp::Compare(const CTimeStamp &x) const {
    if(m_year < x.m_year) {
        return -1;
    } else if(m_year > x.m_year ){
        return 1;
    }
    if(m_month < x.m_month) {
        return -1;
    } else if(m_month > x.m_month ){
        return 1;
    }
    if(m_day < x.m_day) {
        return -1;
    } else if(m_day > x.m_day ){
        return 1;
    }
    if(m_hour < x.m_hour) {
        return -1;
    } else if(m_hour > x.m_hour ){
        return 1;
    }
    if(m_minute < x.m_minute) {
        return -1;
    } else if(m_minute > x.m_minute ){
        return 1;
    }
    if(m_sec < x.m_sec) {
        return -1;
    } else if(m_sec > x.m_sec ){
        return 1;
    }
    return 0;
}

ostream &operator<<(ostream &os, const CTimeStamp &x) {
    string ok  ="2019-03-29 13:36:13.023";
    os << x.m_year << "-";
    os << setfill('0') << setw(2) <<  x.m_month << "-";
    os << setfill('0') << setw(2) << x.m_day << " ";
    os << setfill('0') << setw(2) << x.m_hour << ":";
    os << setfill('0') << setw(2)<< x.m_minute << ":";
    os << setiosflags(ios::fixed) << setprecision(3) << x.m_sec;
    return os;
}

class CMail
{
public:
    CMail                                   ( const CTimeStamp & timeStamp,
                                              const string     & from,
                                              const string     & to,
                                              const string     & subject );
    int            CompareByTime                           ( const CTimeStamp & x ) const;
    int            CompareByTime                           ( const CMail      & x ) const;
    const string & From                                    ( void ) const;
    const string & To                                      ( void ) const;
    const string & Subject                                 ( void ) const;
    const CTimeStamp & TimeStamp                           ( void ) const;
    friend ostream & operator <<                           ( ostream          & os,
                                                             const CMail      & x );
private:
    CTimeStamp timestamp;
    string from;
    string to;
    string subject;
};

ostream &operator<<(ostream &os, const CMail &x) {
    os << x.timestamp << " " << x.from << " -> " << x.to << ", subject: " << x.subject;
    return os;
}

CMail::CMail(const CTimeStamp &timeStamp, const string &from, const string &to, const string &subject)
: timestamp(timeStamp), from(from), to(to),subject(subject)
{

}

const string &CMail::From(void) const {
    return from;
}
const string &CMail::To(void) const {
    return to;
}
// your code will be compiled in a separate namespace
namespace MysteriousNamespace {
#endif /* __PROGTEST__ */
//----------------------------------------------------------------------------------------
    struct LogTransferObject {
        string from;
        vector<pair<string,CTimeStamp>> to;
        string subject;
    };

    struct TimeStampCompare
    {
        bool operator() (const CTimeStamp& lhs, const CTimeStamp& rhs) const
        {
            return lhs.Compare(rhs) < 0;
        }
    };

    class CMailLog
    {
    public:
        int            ParseLog                                ( istream          & in );

        list<CMail>    ListMail                                ( const CTimeStamp & from,
                                                                 const CTimeStamp & to ) const;

        set<string>    ActiveUsers                             ( const CTimeStamp & from,
                                                                 const CTimeStamp & to ) const;
    private:
        void insertIntoLogs(const string & message,const string & code, const CTimeStamp & timestamp);
        bool parseLine(istream &in);
        multimap<CTimeStamp,CMail,TimeStampCompare> emails;
        map<string,LogTransferObject> logs;
    };

    struct getSecond : public std::unary_function<multimap<CTimeStamp,CMail>::value_type, string>
    {
        CMail operator()(const multimap<CTimeStamp,CMail>::value_type& value) const
        {
            return value.second;
        }
    };

    enum LOG_TYPE {
            from = 1,
            to = 2,
            subject = 3,
            ignore = 4,
    };

    enum LOG_TYPE getLogType(const string &message){
        if (message.rfind("from", 0) == 0)
            return from;
        if (message.rfind("to", 0) == 0)
            return to;
        if (message.rfind("subject", 0) == 0)
            return subject;
        return ignore;
    }

    void CMailLog::insertIntoLogs(const string & message,const string & code, const CTimeStamp & timestamp){
        LOG_TYPE  type = getLogType(message);
        if(type == from) {
            const std::string emailAddress = message.substr(message.find('=') + 1);//TODO check
            LogTransferObject log;
            log.from = emailAddress;
            logs.insert(make_pair(code,log));
        } else if( type == to ){
            std::string emailAddress = message.substr(message.find('=') + 1);
            logs[code].to.emplace_back(make_pair(emailAddress, timestamp) );
        } else if(type == subject){
            std::string sub = message.substr(message.find('=') + 1);
            logs[code].subject = sub;
        }
    }

    int parseMonth(const string & str){
        static const map<string, int> months
                    {
                            { "Jan", 1 },
                            { "Feb", 2 },
                            { "Mar", 3 },
                            { "Apr", 4 },
                            { "May", 5 },
                            { "Jun", 6 },
                            { "Jul", 7 },
                            { "Aug", 8 },
                            { "Sep", 9 },
                            { "Oct", 10 },
                            { "Nov", 11 },
                            { "Dec", 12 }
                    };

        auto iter = months.find(str);
        return (iter != months.cend()) ? iter->second : -1;
    }

    bool CMailLog::parseLine(istream &in){
        string dns, code, message, tmo_message;
        string month;
        int day, year, hours, minutes;
        double seconds;
        char delimiter;
        if((in >> month) && (in >> day) && (in >> year) && (in >> hours) && (in >> delimiter) && (in >> minutes) &&
        (in >> delimiter) && (in >> seconds) && (in >> dns) && (in >> code)){

            while (in.peek() == ' ') // skip spaces
                in.get();
            if(!getline(in, message, '\n')){
                // error
            }
            int intMonth = parseMonth(month);
            CTimeStamp timestamp(year,intMonth,day,hours,minutes,seconds);
            insertIntoLogs(message,code,timestamp);
            return true;
        }
        return false;
    }

    int CMailLog::ParseLog(istream &in) {
        while(parseLine(in)) {

        }
        for( auto & log: logs)
            for( auto & item : log.second.to)
                emails.insert(make_pair(item.second, CMail(item.second,log.second.from,item.first,log.second.subject)));

        return emails.size();
    }

    list<CMail> CMailLog::ListMail(const CTimeStamp &from, const CTimeStamp &to) const {
        auto startiter = emails.lower_bound(from);
        auto enditer = emails.upper_bound(to);
        list<CMail> cmails;
        transform(startiter, enditer, back_inserter(cmails), getSecond() );
        return cmails;
    }

    set<string> CMailLog::ActiveUsers(const CTimeStamp &from, const CTimeStamp &to) const {
        auto startiter = emails.lower_bound(from);
        auto enditer = emails.upper_bound(to);
        set<string> emailAdresses;
        for(;startiter != enditer;++startiter){
            string new_str;
            emailAdresses.insert((*startiter).second.From());
            emailAdresses.insert((*startiter).second.To());
        }
        return emailAdresses;
    }
//----------------------------------------------------------------------------------------
#ifndef __PROGTEST__
} // namespace
string             printMail                               ( const list<CMail> & all )
{
    ostringstream oss;
    for ( const auto & mail : all )
        oss << mail << endl;
    return oss . str ();
}
string             printUsers                              ( const set<string> & all )
{
    ostringstream oss;
    bool first = true;
    for ( const auto & name : all )
    {
        if ( ! first )
            oss << ", ";
        else
            first = false;
        oss << name;
    }
    return oss . str ();
}
int                main                                    ( void )
{
    MysteriousNamespace::CMailLog m;
    list<CMail> mailList;
    set<string> users;
    istringstream iss;

    iss . clear ();
    iss . str (
            "Mar 29 2019 12:35:32.233 relay.fit.cvut.cz ADFger72343D: from=user1@fit.cvut.cz\n"
            "Mar 29 2019 12:37:16.234 relay.fit.cvut.cz JlMSRW4232Df: from=person3@fit.cvut.cz\n"
            "Mar 29 2019 12:55:13.023 relay.fit.cvut.cz JlMSRW4232Df: subject=New progtest homework!\n"
            "Mar 29 2019 13:38:45.043 relay.fit.cvut.cz Kbced342sdgA: from=office13@fit.cvut.cz\n"
            "Mar 29 2019 13:36:13.023 relay.fit.cvut.cz JlMSRW4232Df: to=user76@fit.cvut.cz\n"
            "Mar 29 2019 13:55:31.456 relay.fit.cvut.cz KhdfEjkl247D: from=PR-department@fit.cvut.cz\n"
            "Mar 29 2019 14:18:12.654 relay.fit.cvut.cz Kbced342sdgA: to=boss13@fit.cvut.cz\n"
            "Mar 29 2019 14:48:32.563 relay.fit.cvut.cz KhdfEjkl247D: subject=Business partner\n"
            "Mar 29 2019 14:58:32.000 relay.fit.cvut.cz KhdfEjkl247D: to=HR-department@fit.cvut.cz\n"
            "Mar 29 2019 14:25:23.233 relay.fit.cvut.cz ADFger72343D: mail undeliverable\n"
            "Mar 29 2019 15:02:34.231 relay.fit.cvut.cz KhdfEjkl247D: to=CIO@fit.cvut.cz\n"
            "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=CEO@fit.cvut.cz\n"
            "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=dean@fit.cvut.cz\n"
            "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=vice-dean@fit.cvut.cz\n"
            "Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=archive@fit.cvut.cz\n" );
    assert ( m . ParseLog ( iss ) == 8 );
    mailList = m . ListMail ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),
                              CTimeStamp ( 2019, 3, 29, 23, 59, 59 ) );
    assert ( printMail ( mailList ) ==
             "2019-03-29 13:36:13.023 person3@fit.cvut.cz -> user76@fit.cvut.cz, subject: New progtest homework!\n"
             "2019-03-29 14:18:12.654 office13@fit.cvut.cz -> boss13@fit.cvut.cz, subject: \n"
             "2019-03-29 14:58:32.000 PR-department@fit.cvut.cz -> HR-department@fit.cvut.cz, subject: Business partner\n"
             "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> CEO@fit.cvut.cz, subject: Business partner\n"
             "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> dean@fit.cvut.cz, subject: Business partner\n"
             "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> vice-dean@fit.cvut.cz, subject: Business partner\n"
             "2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> archive@fit.cvut.cz, subject: Business partner\n"
             "2019-03-29 15:02:34.231 PR-department@fit.cvut.cz -> CIO@fit.cvut.cz, subject: Business partner\n" );
    mailList = m . ListMail ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),
                              CTimeStamp ( 2019, 3, 29, 14, 58, 32 ) );
    assert ( printMail ( mailList ) ==
             "2019-03-29 13:36:13.023 person3@fit.cvut.cz -> user76@fit.cvut.cz, subject: New progtest homework!\n"
             "2019-03-29 14:18:12.654 office13@fit.cvut.cz -> boss13@fit.cvut.cz, subject: \n"
             "2019-03-29 14:58:32.000 PR-department@fit.cvut.cz -> HR-department@fit.cvut.cz, subject: Business partner\n" );
    mailList = m . ListMail ( CTimeStamp ( 2019, 3, 30, 0, 0, 0 ),
                              CTimeStamp ( 2019, 3, 30, 23, 59, 59 ) );
    assert ( printMail ( mailList ) == "" );
    users = m . ActiveUsers ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),
                              CTimeStamp ( 2019, 3, 29, 23, 59, 59 ) );
    assert ( printUsers ( users ) == "CEO@fit.cvut.cz, CIO@fit.cvut.cz, HR-department@fit.cvut.cz, PR-department@fit.cvut.cz, archive@fit.cvut.cz, boss13@fit.cvut.cz, dean@fit.cvut.cz, office13@fit.cvut.cz, person3@fit.cvut.cz, user76@fit.cvut.cz, vice-dean@fit.cvut.cz" );
    users = m . ActiveUsers ( CTimeStamp ( 2019, 3, 28, 0, 0, 0 ),
                              CTimeStamp ( 2019, 3, 29, 13, 59, 59 ) );
    assert ( printUsers ( users ) == "person3@fit.cvut.cz, user76@fit.cvut.cz" );
    return 0;
}
#endif /* __PROGTEST__ */
